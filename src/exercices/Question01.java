package exercices;


import donnees.Personne;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class Question01 {

  public static void main(String[] args) {
      
      Map<String,Integer> repartVille= new TreeMap(); 
     
      System.out.println("\nNombre de personnes par villes\n");
      
      for( Personne pers : donnees.Club.listeDesPersonnes ){
      
      
          String ville= pers.ville;
          
          
          if ( repartVille.containsKey(ville)){
          
            int nb=repartVille.get(ville)+1;
            repartVille.put(ville, nb);
          }
          else{
            repartVille.put(ville, 1);
          }
      }
      
      
      for (Entry ent : repartVille.entrySet() ){
      
          System.out.printf("%-15s  %3d \n",
                  ent.getKey(),
                  ent.getValue()
                          );
      }
  }
}



