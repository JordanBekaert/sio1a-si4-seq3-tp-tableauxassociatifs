
package exemples.tableassoc;

import java.util.Map;
import java.util.TreeMap;


public class Ex08_TableauAssociatif_AccesDirect {

    public static void main(String[] args) {
       
        // Déclaration du tableau associatif clé de type String valeur de type Integer
        // Les entrées du tableau seront rangés dans l'ordre croissant de la clé ( TreeMap)
        
        Map<String,Integer> resultats=new TreeMap();
        
        resultats.put("Pierre", 12);
        resultats.put("Alain", 10);
        resultats.put("Jacques", 8);
        resultats.put("Fabienne", 16);
        resultats.put("Marie", 13);
        resultats.put("Béatrice", 14);
        resultats.put("Léa", 15);
        resultats.put("Thierry", 16);
        
        // Accès direct à la note de Léa et affichage
       
        System.out.println();
        System.out.println("Léa a obtenu:  "+resultats.get("Léa"));
        System.out.println();
       
        // Modification de la note de Léa
        
        System.out.println("On monte la note de Léa à 17");
        resultats.put("Léa", 17);
        
        // Accès direct à la note de Léa et affichage
        
        System.out.println("Léa a maintenant:  "+resultats.get("Léa"));
        
    }
}
