
package exemples.tableassoc;

import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class Ex10_TableauAssociatif_Repartition {

    public static void main(String[] args) {
       
        
        Integer[ ] notes={
                           12, 4, 8,15,12,10,12, 8,14,13,12,12,14,
                           15,16, 2,11,10,13,16,12,11,19,13, 7,11,
                           20,12,13,14,15, 9, 3, 9,10, 3,16,13,19,
                           13, 8,15, 7, 8,11,18,10, 9,14,15,12,10,
                           15, 8, 6, 4,10,12,16, 8, 4,18,13, 7,12
                         };
        
        Map<Integer, Integer> repart= new TreeMap();
                
        for (Integer n=0; n<=20; n++){ repart.put(n, 0);}
        
        for( Integer n : notes){
        
           Integer val=repart.get(n)+1;
           repart.put(n, val); 
        }
        
        System.out.println("Répartition des notes\n");
        
        for( Entry  e: repart.entrySet()){
        
            System.out.printf("%3d %3d\n",e.getKey(),e.getValue() );
        }
        
        int total=0; for( Integer n : notes) total+=n;
        System.out.printf("\nMoyenne: %5.2f\n\n", (float) total/notes.length );
        
    }
}
