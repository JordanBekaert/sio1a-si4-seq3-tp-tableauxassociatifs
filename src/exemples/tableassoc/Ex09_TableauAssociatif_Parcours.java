
package exemples.tableassoc;

import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class Ex09_TableauAssociatif_Parcours {
   
    public static void main(String[] args) {
       
        // Déclaration du tableau associatif clé de type String valeur de type Integer
        
        Map<String,Integer> resultats=new TreeMap();
        
        
        resultats.put("Pierre", 12);
        resultats.put("Alain", 10);
        resultats.put("Jacques", 8);
        resultats.put("Fabienne", 16);
        resultats.put("Marie", 13);
        resultats.put("Béatrice", 14);
        resultats.put("Léa", 15);
        resultats.put("Thierry", 16);
         
        System.out.println();
        
        
        // Liste des clés

        System.out.println("Liste des clés\n");
        
        for ( String nom :resultats.keySet()) {
        
           System.out.print(nom + "  ");   
        }
        
        System.out.println("\n");
        
        // Liste des valeurs
        
        System.out.println("Liste des valeurs\n");
        
        for ( Integer note :resultats.values()) {
        
           System.out.print(note+ " ");   
        }
        
        System.out.println("\n");
        
        
        // Liste des entrées
        
        System.out.println("Liste des entrées\n");
        
        for ( Entry ent :resultats.entrySet()) {
        
           System.out.println(ent.getKey()+ " "+ ent.getValue());   
        }
        
        System.out.println("\n");
        
        
    }
}
